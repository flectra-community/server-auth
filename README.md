# Flectra Community / server-auth

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[auth_api_key_group](auth_api_key_group/) | 2.0.1.1.1| Allow grouping API keys together.Grouping per se does nothing. This feature is supposed to be used by other modulesto limit access to services or records based on groups of keys.    
[password_security](password_security/) | 2.0.1.0.0| Allow admin to set password security requirements.
[auth_ldaps](auth_ldaps/) | 2.0.1.0.1| Allows to use LDAP over SSL authentication
[auth_api_key_server_env](auth_api_key_server_env/) | 2.0.1.1.0| Configure api keys via server env.This can be very useful to avoid mixing your keys between your variousenvironments when restoring databases. All you have to do is to add a newsection to your configuration file according to the following convention:    
[auth_user_case_insensitive](auth_user_case_insensitive/) | 2.0.1.0.0| Makes the user login field case insensitive
[users_ldap_groups](users_ldap_groups/) | 2.0.1.0.0| Adds user accounts to groups based on rules defined by the administrator.
[auth_dynamic_groups](auth_dynamic_groups/) | 2.0.1.0.0| Have membership conditions for certain groups
[auth_jwt](auth_jwt/) | 2.0.2.0.0|         JWT bearer token authentication.
[user_log_view](user_log_view/) | 2.0.1.0.0| Allow to see user's actions log
[auth_jwt_demo](auth_jwt_demo/) | 2.0.1.2.0|         Test/demo module for auth_jwt.
[auth_admin_passkey](auth_admin_passkey/) | 2.0.1.0.0| Allows system administrator to authenticate with any account
[vault_share](vault_share/) | 2.0.1.1.0| Implementation of a mechanism to share secrets
[auth_session_timeout](auth_session_timeout/) | 2.0.1.0.1|         This module disable all inactive sessions since a given delay
[auth_oidc](auth_oidc/) | 2.0.1.0.2| Allow users to login through OpenID Connect Provider
[users_ldap_mail](users_ldap_mail/) | 2.0.1.0.0| LDAP mapping for user name and e-mail
[auth_api_key](auth_api_key/) | 2.0.2.1.0|         Authenticate http requests from an API key
[auth_signup_verify_email](auth_signup_verify_email/) | 2.0.1.0.1| Force uninvited users to use a good email for signup
[auth_saml](auth_saml/) | 2.0.1.1.0| SAML2 Authentication
[vault](vault/) | 2.0.1.6.1| Password vault integration in Odoo



# Copyright (C) 2020 GlodoUK <https://www.glodo.uk/>
# Copyright (C) 2010-2016, 2022 XCG Consulting <http://flectra.consulting>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "SAML2 Authentication",
    "version": "2.0.1.1.0",
    "category": "Tools",
    "author": "XCG Consulting, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/server-auth",
    "license": "AGPL-3",
    "depends": ["base_setup"],
    "external_dependencies": {
        "python": ["pysaml2"],
        "bin": ["xmlsec1"],
        # special definition used by OCA to install packages
        "deb": ["xmlsec1"],
    },
    "demo": [],
    "data": [
        "security/ir.model.access.csv",
        "views/auth_saml.xml",
        "views/res_config_settings.xml",
        "views/res_users.xml",
        "data/ir_config_parameter.xml",
    ],
    "installable": True,
    "auto_install": False,
    "development_status": "Beta",
}

# Copyright 2021 ACSONE SA/NV
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).

{
    "name": "Auth JWT Test",
    "summary": """
        Test/demo module for auth_jwt.""",
    "version": "2.0.1.2.0",
    "license": "LGPL-3",
    "author": "ACSONE SA/NV,Odoo Community Association (OCA)",
    "maintainers": ["sbidoul"],
    "website": "https://gitlab.com/flectra-community/server-auth",
    "depends": ["auth_jwt"],
    "data": [],
    "demo": ["demo/auth_jwt_validator.xml"],
}

# (c) 2015 ACSONE SA/NV, Dhinesh D
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Inactive Sessions Timeout",
    "summary": """
        This module disable all inactive sessions since a given delay""",
    "author": "ACSONE SA/NV, "
    "Dhinesh D, "
    "Jesse Morgan, "
    "LasLabs, "
    "Odoo Community Association (OCA)",
    "maintainer": "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/server-auth",
    "category": "Tools",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "data": ["data/ir_config_parameter_data.xml"],
    "installable": True,
}
